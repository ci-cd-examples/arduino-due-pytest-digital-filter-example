# Ari Mahpour
# 07/21/2021
# This Pytest module uses the Analog Discovery 2 to demonstrates hardware in the loop (HIL) testing

import pytest
import xunitparser
from serial import Serial
from serial.tools import list_ports
from AD2 import AD2
import matplotlib.pyplot as plt

ARDUINO_DUE_VID = 0x2341
ARDUINO_DUE_PID = 0x003d
BAUD = 115200

# Assumes you have only one device plugged in with the 
# VID and PID specified
# TO DO: Add error handling if device cannot be found
def getPortByVIDPID(VID, PID):
    for port in list_ports.comports():
        if (port.vid == VID and port.pid == PID):
            return port.device

# Drive a waveform using the AD2 at target_freq and read back from DAC channel off DUT
# Validate that the expected_amplitude is met within the tolerance
def analyze_frequency(analog_chan_out, analog_chan_in, frequency, amplitude, tolerance):
    FREQ_MULTIPLIER = 10e3
    NUM_SAMPLES = 100000
    PEAK_SAMPLES = 5

    # First set the output on the AD2
    if pytest.global_variable_2 is not None:
        pytest.global_variable_2.close()
    pytest.global_variable_2 = AD2()
    pytest.global_variable_2.gen_sin_waveform(analog_chan_out, frequency, amplitude, 1)
    samples = pytest.global_variable_2.get_samples(analog_chan_in, FREQ_MULTIPLIER, int(NUM_SAMPLES/frequency*2), pytest.global_variable_2.dwf_ao)
    pytest.global_variable_2.close()

    # TO DO: 1. Chop off first and last 20% of sample
    #        2. Grab a subset of the sample as the frequency increases
    #        (i.e. if frequency is 100 Hz then grab NUM_SAMPLES / 100 around the center)
    num_elements_removed = int(0.2 * len(samples))
    samples = samples[num_elements_removed:-num_elements_removed]
    max_filtered = sum(sorted(samples)[-PEAK_SAMPLES:])/PEAK_SAMPLES
    min_filtered = sum(sorted(samples)[:PEAK_SAMPLES])/PEAK_SAMPLES
    amplitude_filtered = round(abs(max_filtered - min_filtered)/2, 3)
    
    print("Frequency: %d; Amplitude: %f; Peak-to-Peak: %f" % (frequency, amplitude_filtered, amplitude_filtered * 2))
    plt.plot(samples)
    plt.xlabel('Sample')
    plt.ylabel('Amplitude')
    plt.ylim(bottom=0.5, top=2)
    plt.savefig('plots/plot_' + str(frequency) + 'Hz.png')
    plt.close()

    return amplitude_filtered

def setup_module():
    # Ensure that there is an Arduino Due connected
    COMPort = getPortByVIDPID(ARDUINO_DUE_VID, ARDUINO_DUE_PID)
    assert COMPort is not None, "Cannot find an Arduino connected with the specified VID and PID."

    # Configure global handles to hardware devices
    pytest.global_variable_1 = Serial(COMPort, BAUD, timeout=1)
    pytest.global_variable_2 = AD2()

# Verify that calls to the Waveforms software can be made
def test_sw_version():
    assert pytest.global_variable_2.dwf_version == "3.16.3"

# Run through the frequency spectrum and validate that the signal has been attenuated properly
def test_filter():   
    print("\nPerforming frequency sweep.")
    for freq in range(1, 31, 1):
        freq = freq * 10
        amplitude = analyze_frequency(analog_chan_out=0, analog_chan_in=1, frequency=freq, amplitude=1, tolerance=0)
        assert amplitude < 0.2 if freq >= 100 else True, "Amplitude is greater than 0.2 for frequencies above 100 Hz"