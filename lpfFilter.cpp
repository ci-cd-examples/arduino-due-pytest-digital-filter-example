// Arduino Signal Filtering Library
// Copyright 2012-2015 Jeroen Doggen (jeroendoggen@gmail.com)

#include <Arduino.h>
#include "lpfFilter.h"

/// Constructor
lpfFilter::lpfFilter()
{
    v[0]=0;
    v[1]=0;
}

/// Begin function: set default filter options
void lpfFilter::begin()
{
}

/// Just a placeholder
float lpfFilter::step(float x)
{
    v[0] = v[1];
    v[1] = (3.131764229192701265e-3 * x)
         + (0.99373647154161459660 * v[0]);
    return 
        (v[0] + v[1]);
}
