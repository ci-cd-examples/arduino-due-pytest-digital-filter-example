# Filename: AD2.py
# Author: Ari Mahpour
# Date created: 06/27/2021
# Description: Helper class to run various functions off the Analog Discovery 2 device
# Source: https://github.com/amuramatsu/dwf

import dwf
import time
import sys

class AD2:
    def __init__(self):
        # Connect to software and get version
        self.dwf_version = dwf.FDwfGetVersion()

        # Handle to analog out
        self.dwf_ao = dwf.DwfAnalogOut()

    def gen_sin_waveform(self, channel, frequency, amplitude, offset):
        self.dwf_ao.nodeEnableSet(channel, self.dwf_ao.NODE.CARRIER, True)
        self.dwf_ao.nodeFunctionSet(channel, self.dwf_ao.NODE.CARRIER, self.dwf_ao.FUNC.SINE)
        self.dwf_ao.nodeFrequencySet(channel, self.dwf_ao.NODE.CARRIER, frequency)
        self.dwf_ao.nodeAmplitudeSet(channel, self.dwf_ao.NODE.CARRIER, amplitude)
        self.dwf_ao.nodeOffsetSet(channel, self.dwf_ao.NODE.CARRIER, offset)
        self.dwf_ao.configure(channel, True)
    
    def gen_dc(self, channel, offset):
        self.dwf_ao.nodeEnableSet(channel, self.dwf_ao.NODE.CARRIER, True)
        self.dwf_ao.nodeFunctionSet(channel, self.dwf_ao.NODE.CARRIER, self.dwf_ao.FUNC.DC)
        self.dwf_ao.nodeOffsetSet(channel, self.dwf_ao.NODE.CARRIER, offset)
        self.dwf_ao.configure(channel, True)

    # f_s: Sampling frequency
    # num_samples: Number of samples to read
    def get_samples(self, channel, f_s, num_samples, reference=None):
        # Set up acquisition
        self.dwf_ai = dwf.DwfAnalogIn(reference)
        self.dwf_ai.channelEnableSet(channel, True)
        self.dwf_ai.channelRangeSet(channel, 5.0)
        self.dwf_ai.acquisitionModeSet(self.dwf_ai.ACQMODE.RECORD)
        self.dwf_ai.frequencySet(f_s)
        self.dwf_ai.recordLengthSet(num_samples / f_s)

        # Wait at least 2 seconds for the offset to stabilize
        time.sleep(2)

        # Begin acquisition
        self.dwf_ai.configure(False, True)

        rgdSamples = []
        cSamples = 0
        fLost = False
        fCorrupted = False
        while cSamples < num_samples:
            sts = self.dwf_ai.status(True)
            if cSamples == 0 and sts in (self.dwf_ai.STATE.CONFIG,
                                        self.dwf_ai.STATE.PREFILL,
                                        self.dwf_ai.STATE.ARMED):
                # Acquisition not yet started.
                continue

            cAvailable, cLost, cCorrupted = self.dwf_ai.statusRecord()
            cSamples += cLost
                
            if cLost > 0:
                fLost = True
            if cCorrupted > 0:
                fCorrupted = True
            if cAvailable == 0:
                continue
            if cSamples + cAvailable > num_samples:
                cAvailable = num_samples - cSamples
            
            # get samples
            rgdSamples.extend(self.dwf_ai.statusData(0, cAvailable))
            cSamples += cAvailable

        if fLost:
            print("Samples were lost! Reduce frequency")
        if cCorrupted:
            print("Samples could be corrupted! Reduce frequency")

        return rgdSamples

    def stop_waveform(self, channel):
        self.dwf_ao.configure(channel, False)

    def close(self):
        self.dwf_ao.close()

## Example main to demonstrate use of functions
if __name__ == '__main__':
    import time

    CHANNEL = 1
    AMPLITUDE = 2
    OFFSET = 2

    device = AD2()
    for i in range(1000, 10000, 1000):
        print("Generating sine wave at", i, "Hz")
        device.gen_sin_waveform(CHANNEL, i, AMPLITUDE, OFFSET)
        time.sleep(1)
        device.gen_dc(CHANNEL, 0)
        time.sleep(3)

    device.close()