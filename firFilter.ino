// Arduino Signal Filtering Library: fir filter example
// Copyright 2012-2015 Jeroen Doggen (jeroendoggen@gmail.com)

#include "lpfFilter.h"

#define ADC_SCALE 3.3/1023.0

lpfFilter Filter;

float sensorValue;
float sensorVoltage;
float filteredValue;
float filteredVoltage;

void setup()
{
  Serial.begin(115200);
  pinMode(DAC0, OUTPUT);
  pinMode(DAC1, OUTPUT);
  pinMode(A0, INPUT);
  Filter.begin();
}

void loop()
{
  // Read A0 input
  sensorValue = analogRead(A0);
  sensorVoltage = sensorValue * ADC_SCALE;

  // Apply the digital filter
  filteredValue = Filter.step(sensorValue);
  filteredVoltage = filteredValue * ADC_SCALE;

  // Write the raw and filtered voltages out to the DAC
  analogWrite(DAC0, sensorValue/4);
  analogWrite(DAC1, filteredValue/4);
}
