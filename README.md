# Arduino Due Pytest Digital Filter Example

Demonstration of digital filtering and verification using an Arduino Due

## Hook Up Guide
| Arduino  | Analog Discovery 2 |
| -------- | ------------------ |
| A0       | W1                 |
| GND      | GND                |
| DAC1     | 1+                 |
| GND      | 1-                 |

## How to Run
Compile/Program the Arduino:
```
python3 -m pytest -s test_arduino_load.py
```

Run the filter test:
```
python3 -m pytest -s test_arduino_hil.py
```