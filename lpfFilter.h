// Arduino Signal Filtering Library
// Copyright 2012-2015 Jeroen Doggen (jeroendoggen@gmail.com)

#ifndef lpfFilter_h
#define lpfFilter_h
#include <Arduino.h>

class lpfFilter
{
  public:
    lpfFilter();
    void begin();

    float step(float x);

  private:
    float v[2];
};
#endif
